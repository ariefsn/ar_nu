module.exports = {
  mode: 'development',
  entry: __dirname + '/assets/js/main.js',
  output: {
    path: __dirname + '/assets/js',
    filename: 'bundle.js',
    libraryTarget: 'window'
  },
  module: {
    rules: [{
      loader: 'babel-loader',
      test: /\.js$/,
      exclude: /node_modules/,
      options: {
        presets: ['@babel/preset-env']
      }
    }]
  },
  devServer: {
    port: 3000,
    contentBase: __dirname + '/assets/js',
    inline: true
  }
}