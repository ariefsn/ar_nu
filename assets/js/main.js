const listMenu = ['about', 'setting', 'option1', 'option2', 'option3']
const listField = ['name', 'website', 'phone', 'location']

export const changeMenu = (menu) => {
  listMenu.forEach(m => {
    const element = document.querySelector('#' + m)
    element.style.display = m === menu ? 'block' : 'none'
  })
}

export const showPopup = (field) => {
  listField.forEach(f => {
    const element = document.querySelector('#popup-' + f)
    if (f === field) {
      element.style.cssText = 'display: inline-block !important'
      let oldVal = document.querySelector(`.content .paragraph .${f} span`).innerHTML
      document.querySelector('.input-'+f).value = oldVal
    } else {
      element.style.cssText = 'display: none !important'
    }
  })
}

export const closePopup = (field) => {
  const element = document.querySelector('#popup-' + field)
  element.style.cssText = 'display: none !important'
}

export const changeMode = (mode, content) => {
  const elementEdit = document.querySelector(`#${content}-edit`)
  const elementAction = document.querySelector(`#${content}-action`)
  if (mode === 'edit') {
    elementEdit.style.cssText = 'display: none !important'
    elementAction.style.cssText = 'display: inline-block !important'

    let name = document.querySelector(`.content .paragraph .name span`).innerHTML.split(' ')
    let firstName = name[0]
    let lastName = name[1]
    let website = document.querySelector(`.content .paragraph .website span`).innerHTML
    let phone = document.querySelector(`.content .paragraph .phone span`).innerHTML
    let location = document.querySelector(`.content .paragraph .location span`).innerHTML

    // Name
    document.querySelector(`#wrapper-name`).style.cssText = 'display: none !important'
    document.querySelector(`#wrapper-input-name`).style.cssText = 'display: block !important'
    document.querySelector('.input-first-name').value = firstName
    document.querySelector('.input-last-name').value = lastName
    // Website
    document.querySelector(`#wrapper-website`).style.cssText = 'display: none !important'
    document.querySelector(`#wrapper-input-website`).style.cssText = 'display: block !important'
    document.querySelector('.input-website-m').value = website
    // Phone
    document.querySelector(`#wrapper-phone`).style.cssText = 'display: none !important'
    document.querySelector(`#wrapper-input-phone`).style.cssText = 'display: block !important'
    document.querySelector('.input-phone-m').value = phone
    // Location
    document.querySelector(`#wrapper-location`).style.cssText = 'display: none !important'
    document.querySelector(`#wrapper-input-location`).style.cssText = 'display: block !important'
    document.querySelector('.input-location-m').value = location
  } else {
    elementEdit.style.cssText = 'display: inline-block !important'
    elementAction.style.cssText = 'display: none !important'
    document.querySelector(`#wrapper-name`).style.cssText = 'display: block !important'
    document.querySelector(`#wrapper-website`).style.cssText = 'display: block !important'
    document.querySelector(`#wrapper-phone`).style.cssText = 'display: block !important'
    document.querySelector(`#wrapper-location`).style.cssText = 'display: block !important'

    document.querySelector(`#wrapper-input-name`).style.cssText = 'display: none !important'
    document.querySelector(`#wrapper-input-website`).style.cssText = 'display: none !important'
    document.querySelector(`#wrapper-input-phone`).style.cssText = 'display: none !important'
    document.querySelector(`#wrapper-input-location`).style.cssText = 'display: none !important'
  }
}