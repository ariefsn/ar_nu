(function(e, a) { for(var i in a) e[i] = a[i]; }(window, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/js/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/js/main.js":
/*!***************************!*\
  !*** ./assets/js/main.js ***!
  \***************************/
/*! exports provided: changeMenu, showPopup, closePopup, changeMode */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"changeMenu\", function() { return changeMenu; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"showPopup\", function() { return showPopup; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"closePopup\", function() { return closePopup; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"changeMode\", function() { return changeMode; });\nvar listMenu = ['about', 'setting', 'option1', 'option2', 'option3'];\nvar listField = ['name', 'website', 'phone', 'location'];\nvar changeMenu = function changeMenu(menu) {\n  listMenu.forEach(function (m) {\n    var element = document.querySelector('#' + m);\n    element.style.display = m === menu ? 'block' : 'none';\n  });\n};\nvar showPopup = function showPopup(field) {\n  listField.forEach(function (f) {\n    var element = document.querySelector('#popup-' + f);\n\n    if (f === field) {\n      element.style.cssText = 'display: inline-block !important';\n      var oldVal = document.querySelector(\".content .paragraph .\".concat(f, \" span\")).innerHTML;\n      document.querySelector('.input-' + f).value = oldVal;\n    } else {\n      element.style.cssText = 'display: none !important';\n    }\n  });\n};\nvar closePopup = function closePopup(field) {\n  var element = document.querySelector('#popup-' + field);\n  element.style.cssText = 'display: none !important';\n};\nvar changeMode = function changeMode(mode, content) {\n  var elementEdit = document.querySelector(\"#\".concat(content, \"-edit\"));\n  var elementAction = document.querySelector(\"#\".concat(content, \"-action\"));\n\n  if (mode === 'edit') {\n    elementEdit.style.cssText = 'display: none !important';\n    elementAction.style.cssText = 'display: inline-block !important';\n    var name = document.querySelector(\".content .paragraph .name span\").innerHTML.split(' ');\n    var firstName = name[0];\n    var lastName = name[1];\n    var website = document.querySelector(\".content .paragraph .website span\").innerHTML;\n    var phone = document.querySelector(\".content .paragraph .phone span\").innerHTML;\n    var location = document.querySelector(\".content .paragraph .location span\").innerHTML; // Name\n\n    document.querySelector(\"#wrapper-name\").style.cssText = 'display: none !important';\n    document.querySelector(\"#wrapper-input-name\").style.cssText = 'display: block !important';\n    document.querySelector('.input-first-name').value = firstName;\n    document.querySelector('.input-last-name').value = lastName; // Website\n\n    document.querySelector(\"#wrapper-website\").style.cssText = 'display: none !important';\n    document.querySelector(\"#wrapper-input-website\").style.cssText = 'display: block !important';\n    document.querySelector('.input-website-m').value = website; // Phone\n\n    document.querySelector(\"#wrapper-phone\").style.cssText = 'display: none !important';\n    document.querySelector(\"#wrapper-input-phone\").style.cssText = 'display: block !important';\n    document.querySelector('.input-phone-m').value = phone; // Location\n\n    document.querySelector(\"#wrapper-location\").style.cssText = 'display: none !important';\n    document.querySelector(\"#wrapper-input-location\").style.cssText = 'display: block !important';\n    document.querySelector('.input-location-m').value = location;\n  } else {\n    elementEdit.style.cssText = 'display: inline-block !important';\n    elementAction.style.cssText = 'display: none !important';\n    document.querySelector(\"#wrapper-name\").style.cssText = 'display: block !important';\n    document.querySelector(\"#wrapper-website\").style.cssText = 'display: block !important';\n    document.querySelector(\"#wrapper-phone\").style.cssText = 'display: block !important';\n    document.querySelector(\"#wrapper-location\").style.cssText = 'display: block !important';\n    document.querySelector(\"#wrapper-input-name\").style.cssText = 'display: none !important';\n    document.querySelector(\"#wrapper-input-website\").style.cssText = 'display: none !important';\n    document.querySelector(\"#wrapper-input-phone\").style.cssText = 'display: none !important';\n    document.querySelector(\"#wrapper-input-location\").style.cssText = 'display: none !important';\n  }\n};\n\n//# sourceURL=webpack:///./assets/js/main.js?");

/***/ })

/******/ })));