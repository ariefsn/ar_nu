##### [Live Demo](http://profile-app.ariefsn.my.id)

1. Install Dependency
  ```shell
    npm install
    # or
    yarn install
  ```

2. Build App
  ```shell
    npm run go
    # or
    yarn go
  ```

3. Copy `index.html` and `assets` directory to server host.